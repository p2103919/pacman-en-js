# Pacman en JS



## Projet de JS organisé par Belfadel Adbelahdi


### Participants:

- Viggo Casciano
- Noé Chouteau

## Note pour le correcteur:

Notre projet est constitué des langages: [HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language),[CSS](https://fr.wikipedia.org/wiki/Feuilles_de_style_en_cascade) et [JavaScript](https://fr.wikipedia.org/wiki/JavaScript)

Celui-ci à pour but de re-faire à notre façon le très connu [PACMAN](https://fr.wikipedia.org/wiki/Pac-Man) en utilisant JavaScript avec un Canvas.