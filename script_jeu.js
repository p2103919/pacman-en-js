var tailleBlock = 25;
var ligne = 25;
var colonne = 25;
var canvas;
var context;

var nomJoueur;
var score1 = localStorage.getItem("score");
var score2 = localStorage.getItem("score2");
var score3= localStorage.getItem("score3");
var score4= localStorage.getItem("score4");
var score5= localStorage.getItem("score5");

var name1 =localStorage.getItem("name");
var name2=localStorage.getItem("name2");
var name3=localStorage.getItem("name3");
var name4=localStorage.getItem("name4");
var name5=localStorage.getItem("name5");

if (score1==null && score2==null && score3==null && score4==null && score5==null){
  localStorage.setItem("score",0);
  localStorage.setItem("name","");
  localStorage.setItem("score2",0);
  localStorage.setItem("name2","");
  localStorage.setItem("score3",0);
  localStorage.setItem("name3","");
  localStorage.setItem("score4",0);
  localStorage.setItem("name4","");
  localStorage.setItem("score5",0);
  localStorage.setItem("name5","");
}

// var pour pacman
var pacman = new Image();
pacman.src = "assets/pacman_sprits/pacman_f.png"
var pacman_x = 12 * tailleBlock;
var pacman_y = 13 * tailleBlock;
var vitesse_X = 0;
var vitesse_Y = 0;
var scorePacman = 0;
var firstmove = false;
var viePacman = 3;
var powerUp = false;
var timerPowerUp = 30;
var caseParcouru = 0;

// var pour les fantomes globaux
var fantomeMaisonRouge = true;
var fantomeMaisonRose = true;
var fantomeMaisonBleu = true;
var fantomeMaisonOrange = true;
var chaseTargetRouge;
var chaseTargetRose;
var chaseTargetBleu;
var chaseTargetOrange;


// var pour le fantome rouge
var rouge = new Image();
rouge.src = "assets/ghost_sprits/red_r.png";
var rouge_x = 11 * tailleBlock;
var rouge_y = 9* tailleBlock;
var rouge_vitesse_X = 0;
var rouge_vitesse_Y = 0;
var timerRouge = 10;

//var pour le fantome bleu
var bleu = new Image();
bleu.src = "assets/ghost_sprits/blue_r.png";
var bleu_x = 10 * tailleBlock;
var bleu_y = 9 * tailleBlock;
var bleu_vitesse_X = 0;
var bleu_vitesse_Y = 0;
var timerBleu = 30;

//var pour le fantome rose
var rose = new Image();
rose.src = "assets/ghost_sprits/pink_l.png";
var rose_x = 13 * tailleBlock;
var rose_y = 9 * tailleBlock;
var rose_vitesse_X = 0;
var rose_vitesse_Y = 0;
var timerRose = 60;

//var pour le fantome orange
var g_orange = new Image();
g_orange.src = "assets/ghost_sprits/orange_l.png";
var orange_x = 14 * tailleBlock;
var orange_y = 9 * tailleBlock;
var orange_vitesse_X = 0;
var orange_vitesse_Y = 0;
var timerOrange = 90;

var compteur=0;

//fruit
var timerFruit = 20000;
var fruit;

// variable image
var cerise = new Image();
cerise.src = "assets/fruits/cerise.png";
var pomme = new Image();
pomme.src = "assets/fruits/apple.png";
var cloche = new Image();
cloche.src = "assets/fruits/bell.png";
var fraise = new Image();
fraise.src = "assets/fruits/strawberry.png";
var orange = new Image();
orange.src = "assets/fruits/orange.png";
var clefs = new Image();
clefs.src = "assets/fruits/keys.png";
var vaisseau = new Image();
vaisseau.src = "assets/fruits/spaceship.png";

// bordure est un tableau qui contient les coordonnées de chaque bloc de la bordure qui fait le tour du canvas
var bordure = [[0, 0],[0, 1],[0, 2],[0, 3],[0, 4],[0, 5],[0, 6],[0, 7],[0, 8],[0, 9],[0, 10],[0, 14],[0, 15],[0, 16],[0, 17],[0, 18],[0, 19]
              ,[0, 20],[0, 21],[0, 22],[0, 23],[0, 24],[1, 0],[2, 0],[3, 0],[4, 0],[5, 0],[6, 0],[7, 0],[8, 0],[9, 0],[10, 0],[11, 0],[12, 0]
              ,[13, 0],[14, 0],[15, 0],[16, 0],[17, 0],[18, 0],[19, 0],[20, 0],[21, 0],[22, 0],[23, 0],[24, 0],[1, 24],[2, 24],[3, 24],[4, 24]
              ,[5, 24],[19, 24],[20, 24],[21, 24],[22, 24],[23, 24],[24, 24],[24, 1],[24, 2],[24, 3],[24, 4],[24, 5],[24, 6],[24, 7],[24, 8]
              ,[24, 9],[24, 10],[24, 14],[24, 15],[24, 16],[24, 17],[24, 18],[24, 19],[24, 20],[24, 21],[24, 22],[24, 23],[24, 24],[12, 1]
              ,[12, 2],[12, 3],[1, 10],[2, 10],[1, 14],[2, 14],[22, 10],[23, 10],[22, 14],[23, 14],[12, 21],[6, 23],[6, 23],[7, 23],[8, 23]
              ,[9, 23],[15, 23],[16, 23],[17, 23],[18, 23],[18, 24],[6, 24],[9, 22],[10, 22],[11, 22],[12, 22],[13, 22],[14, 22],[15, 22]
              ,[0, 11],[1, 11],[2, 11],[0, 13],[1, 13],[2, 13],[24, 11],[23, 11],[22, 11],[24, 13],[23, 13],[22, 13]];

var bordure_interne = [[2, 2],[2, 3],[3, 2],[4, 2],[2, 4],[2, 5],[4, 5],[3, 5],[4, 4],[4, 3],[6, 2],[6, 3],[6, 4],[7, 2],[8, 2],[7, 4]
                      ,[8, 4],[8, 3],[10, 2],[10, 3],[10, 4],[14, 2],[14, 3],[14, 4],[16, 2],[16, 4],[16, 3],[17, 2],[17, 4],[21, 2],[21, 5]
                      ,[20, 5],[22, 5],[18, 2],[18, 3],[18, 4],[20, 2],[20, 4],[20, 3],[22, 2],[22, 3],[22, 4],[2, 7],[3, 7],[9, 8],[10, 8]
                      ,[11, 8],[13, 8],[14, 8],[15, 8],[9, 9],[15, 9],[9, 10],[15, 10],[10, 10],[11, 10],[6, 6],[12, 10],[13, 10],[14, 10]
                      ,[22, 7],[21, 7],[6, 7],[6, 8],[6, 9],[6, 10],[4, 7],[4, 8],[4, 9],[4, 10],[3, 8],[2, 8],[7, 6],[8, 6],[9, 6],[10, 6]
                      ,[11, 6],[12, 6],[13, 6],[14, 6],[15, 6],[16, 6],[17, 6],[18, 6],[18, 7],[18, 8],[18, 9],[18, 10],[8, 8],[7, 10]
                      ,[17, 10],[16, 8],[20, 10],[20, 9],[20, 8],[20, 7],[21, 8],[22, 8],[12, 5],[2, 16],[3, 16],[4, 16],[2, 17],[2, 18]
                      ,[3, 18],[4, 18],[4, 17],[3, 20],[2, 20],[4, 20],[2, 21],[4, 21],[2, 22],[3, 22],[4, 22],[6, 21],[7, 21],[8, 20]
                      ,[9, 20],[10, 20],[11, 19],[11, 18],[11, 17],[11, 16],[11, 15],[13, 15],[13, 16],[13, 17],[13, 18],[13, 19],[14, 20]
                      ,[15, 20],[16, 20],[17, 21],[18, 21],[6, 19],[7, 18],[8, 18],[9, 18],[9, 16],[9, 17],[9, 15],[18, 19],[17, 18]
                      ,[16, 18],[15, 18],[15, 16],[15, 17],[15, 15],[6, 16],[7, 15],[6, 15],[7, 16],[6, 14],[7, 14],[9, 14],[11, 14]
                      ,[13, 14],[15, 14],[4, 14],[17, 14],[18, 14],[20, 14],[17, 15],[18, 15],[18, 16],[17, 16],[20, 16],[21, 16],[22, 16]
                      ,[20, 17],[22, 17],[20, 18],[21, 18],[22, 18],[20, 20],[21, 20],[22, 20],[20, 21],[22, 21],[20, 22],[21, 22],[22, 22]
                      ,[4, 11],[4, 13],[6, 12],[7, 12],[8, 12],[9, 12],[10, 12],[11, 12],[12, 12],[13, 12],[14, 12],[15, 12],[16, 12]
                      ,[17, 12],[18, 12],[20, 11],[20, 13]];


// créer un tableau avec toutes les cases vides
var tableau_nourriture = [[1, 1],[1, 2],[1, 3],[1, 4],[1, 5],[1, 6],[1, 7],[1, 8],[1, 9],[1, 15],[1, 16],[1, 17],[1, 18],[1, 19],[1, 20]
,[1, 21],[1, 22],[1, 23],[2, 1],[2, 6],[2, 9],[2, 15],[2, 19],[2, 23],[3, 1],[3, 9],[3, 10],[3, 14],[3, 15],[3, 19]
,[3, 23],[4, 1],[4, 6],[4, 15],[4, 19],[4, 23],[5, 1],[5, 2],[5, 3],[5, 4],[5, 5],[5, 6],[5, 7],[5, 8],[5, 9],[5, 10]
,[5, 14],[5, 15],[5, 16],[5, 17],[5, 18],[5, 19],[5, 20],[5, 22],[5, 23],[6, 1],[6, 5],[6, 18],[6, 17],[6, 20],[6, 22]
,[7, 1],[7, 5],[7, 7],[7, 8],[7, 9],[7, 17],[7, 20],[7, 19],[7, 22],[8, 1],[8, 5],[8, 7],[8, 9],[8, 10],[8, 14],[8, 15]
,[8, 16],[8, 17],[8, 19],[8, 21],[8, 22],[9, 1],[9, 2],[9, 3],[9, 4],[9, 5],[9, 7],[9, 19],[9, 21],[10, 1],[10, 5],[10, 7]
,[10, 14],[10, 15],[10, 16],[10, 17],[10, 18],[10, 19],[10, 21],[11, 1],[11, 2],[11, 3],[11, 4],[11, 5],[11, 7]
,[11, 20],[11, 21],[12, 4],[12, 7],[12, 14],[12, 15],[12, 16],[12, 17],[12, 18],[12, 19],[12, 20],[13, 1],[13, 2],[13, 3]
,[13, 4],[13, 5],[13, 7],[13, 20],[13, 21],[14, 1],[14, 5],[14, 7],[14, 14],[14, 15],[14, 16],[14, 17],[14, 18]
,[14, 19],[14, 21],[15, 1],[15, 2],[15, 3],[15, 4],[15, 5],[15, 7],[15, 19],[15, 21],[16, 1],[16, 5],[16, 7],[16, 9]
,[16, 10],[16, 14],[16, 15],[16, 16],[16, 17],[16, 19],[16, 21],[16, 22],[17, 1],[17, 5],[17, 7],[17, 8],[17, 9],[17, 17]
,[17, 20],[17, 19],[17, 22],[18, 1],[18, 5],[18, 17],[18, 18],[18, 20],[18, 22],[19, 1],[19, 2],[19, 3],[19, 4],[19, 5]
,[19, 6],[19, 7],[19, 8],[19, 9],[19, 10],[19, 14],[19, 15],[19, 16],[19, 17],[19, 18],[19, 19],[19, 20],[19, 22],[20, 1]
,[20, 6],[20, 15],[20, 19],[19, 23],[20, 23],[21, 1],[21, 9],[21, 10],[21, 14],[21, 15],[21, 19],[21, 23],[22, 1],[22, 6]
,[22, 9],[22, 15],[22, 19],[22, 23],[23, 1],[23, 2],[23, 3],[23, 4],[23, 5],[23, 6],[23, 7],[23, 8],[23, 9],[23, 15],[23, 16]
,[23, 17],[23, 18],[23, 19],[23, 20],[23, 21],[23, 22],[23, 23],[3, 11],[3, 12],[3, 13],[4, 12],[5, 11],[5, 12],[5, 13],[6, 11]
,[6, 13],[7, 11],[7, 13],[8, 11],[8, 13],[9, 11],[9, 13],[10, 11],[10, 13],[11, 11],[11, 13],[12, 11],[13, 11],[13, 13],[14, 11]
,[14, 13],[15, 11],[15, 13],[16, 11],[16, 13],[17, 11],[17, 13],[18, 11],[18, 13],[19, 11],[19, 12],[19, 13],[20, 12],[21, 12]
,[21, 11],[21, 13]];


var tableau_powerUp = [
  [3, 6],
  [21, 6],
  [19, 21],
  [5, 21]
];

var tableau_fruit=[["x"]];

var tableau_barriere = [[12, 8]];


window.onload = function () {
  canvas = document.getElementById("canvas");
  canvas.height = ligne * tailleBlock;
  canvas.width = colonne * tailleBlock;
  context = canvas.getContext("2d"); // permet de dessiner sur le canvas

  document.addEventListener("keyup", mouvement);
  setInterval(update, 3000 / 15); // appel de la fonction update toutes les 15ms
};

function update() {
  //créer le fond noir du canvas
  context.fillStyle = "black";
  context.fillRect(0, 0, canvas.width, canvas.height);

  // créer les murs bleus externes 
  for (let i = 0; i < bordure.length; i++) {
    context.fillStyle = "blue";
    context.fillRect(
      bordure[i][0] * tailleBlock,
      bordure[i][1] * tailleBlock,
      tailleBlock - 1,
      tailleBlock - 1
    );
  }
  pacmanTouchGhost();
  // créer les murs internes
  for (let i = 0; i < bordure_interne.length; i++) {
    context.fillStyle = "rgba(35, 76, 161, 1)";
    context.fillRect(
      bordure_interne[i][0] * tailleBlock,
      bordure_interne[i][1] * tailleBlock,
      tailleBlock - 1,
      tailleBlock - 1
    );
  }

  //créer la petite barriere que les ghosts peuvent traverser
  for (let i = 0; i < tableau_barriere.length; i++) {
    context.fillStyle = "rgb(255, 102, 204)";
    context.fillRect(
      tableau_barriere[i][0] * tailleBlock,
      tableau_barriere[i][1] * tailleBlock,
      tailleBlock - 1,
      tailleBlock - 21
    );
  }

  //créer toutes les nourritures sur le terrain
  for (let i = 0; i < tableau_nourriture.length; i++) {
    if (tableau_nourriture[i] != "" && tableau_nourriture[i] != "x" && tableau_nourriture != "xx") {
      context.fillStyle = "white";
      context.fillRect(
        tableau_nourriture[i][0] * tailleBlock + tailleBlock / 2 - 2,
        tableau_nourriture[i][1] * tailleBlock + tailleBlock / 2 - 2,
        tailleBlock - 20,
        tailleBlock - 20
      );
    }
  // créer tous les powerUp sur le terrain
    for (let i = 0; i < tableau_powerUp.length; i++) {
      if (tableau_powerUp[i] != "" && tableau_powerUp[i] != "x") {
        context.fillStyle = "pink";
        context.fillRect(
          tableau_powerUp[i][0] * tailleBlock + tailleBlock / 2 - 6,
          tableau_powerUp[i][1] * tailleBlock + tailleBlock / 2 - 6,
          tailleBlock - 13,
          tailleBlock - 13
        );
      }
    }

  //
  //après le premier mouvement du jeu, on lance la fonction qui va faire apparaitre un fruit puis on l'affiche
    if (firstmove == true) {
      spawnFruit();
      if(tableau_fruit[0][0] == 1 ){
        context.drawImage(pomme, tableau_fruit[0][1] * tailleBlock, tableau_fruit[0][2]* tailleBlock, tailleBlock, tailleBlock);
      }
      if(tableau_fruit[0][0] == 2 ){
        context.drawImage(fraise, tableau_fruit[0][1] * tailleBlock, tableau_fruit[0][2]* tailleBlock, tailleBlock, tailleBlock);
      }
      if(tableau_fruit[0][0] == 3 ){
        context.drawImage(orange, tableau_fruit[0][1] * tailleBlock, tableau_fruit[0][2]* tailleBlock, tailleBlock, tailleBlock);
      }
      if(tableau_fruit[0][0] == 4 ){
        context.drawImage(cerise, tableau_fruit[0][1] * tailleBlock, tableau_fruit[0][2]* tailleBlock, tailleBlock, tailleBlock);
      }
      if(tableau_fruit[0][0] == 5 ){
        context.drawImage(vaisseau, tableau_fruit[0][1] * tailleBlock, tableau_fruit[0][2]* tailleBlock, tailleBlock, tailleBlock);
      }
      if(tableau_fruit[0][0] == 6 ){
        context.drawImage(cloche, tableau_fruit[0][1] * tailleBlock, tableau_fruit[0][2]* tailleBlock, tailleBlock, tailleBlock);
      }
      if(tableau_fruit[0][0] == 7 ){
        context.drawImage(clefs, tableau_fruit[0][1] * tailleBlock, tableau_fruit[0][2]* tailleBlock, tailleBlock, tailleBlock);
      }
    }
  }


  // set le score de pacman selon les nourriture et les powerUp manquants
  scorePacman = score(tableau_nourriture, tableau_powerUp);
  //dessine le fantome rouge
  context.drawImage(rouge,rouge_x, rouge_y, tailleBlock, tailleBlock);
    //dessine le fantome bleu
  context.drawImage(bleu,bleu_x, bleu_y, tailleBlock, tailleBlock);
  // dessine le fantome rose
  context.drawImage(rose,rose_x, rose_y, tailleBlock, tailleBlock);
    // dessine le fantome orange
  context.drawImage(g_orange,orange_x, orange_y, tailleBlock, tailleBlock);

  if (firstmove == true) {
    compter();
    trouverDeplacementFantome();
    deplacementFantome();
  }
  deplacementPacman();
  //
  // utilise la fonction freeCase pour etre sur que pacman ne traverse pas les murs


  

  //
  // regarde si pacman recupere de la nourriture
  if (parcourir_tableau(tableau_nourriture,pacman_x / tailleBlock,pacman_y / tailleBlock)) {
    replace(tableau_nourriture, pacman_x / tailleBlock, pacman_y / tailleBlock);
  }
  
  //
  // regarde si pacman passe sur le fruit, le fait disparaitre pour en generer un nouveau et ajoute des points au score
  if (parcourir_tableau_fruit(tableau_fruit, pacman_x / tailleBlock, pacman_y / tailleBlock)) {
    if (tableau_fruit[0][0] == 1){
      context.fillText("+100", pacman_x, (pacman_y/tailleBlock +2 ) * tailleBlock);
      scorePacman += 100;
    }
    else if (tableau_fruit[0][0] == 2){
      context.fillText("+300", pacman_x, (pacman_y/tailleBlock +2 ) * tailleBlock);
      scorePacman += 300;
    }
    else if (tableau_fruit[0][0] == 3){
      context.fillText("+500", pacman_x, (pacman_y/tailleBlock +2 ) * tailleBlock);
      scorePacman += 500;
    }
    else if (tableau_fruit[0][0] == 4) {
      context.fillText("+700", pacman_x, (pacman_y/tailleBlock +2 ) * tailleBlock);
      scorePacman += 700;
    }
    else if (tableau_fruit[0][0] == 5){
      context.fillText("+2000", pacman_x, (pacman_y/tailleBlock +2 ) * tailleBlock);
      scorePacman += 2000;
    }
    else if (tableau_fruit[0][0] == 6){
      context.fillText("+3000", pacman_x, (pacman_y/tailleBlock +2 ) * tailleBlock);
      scorePacman += 3000;
    }
    else if (tableau_fruit[0][0] == 7){
      context.fillText("+5000", pacman_x, (pacman_y/tailleBlock +2 ) * tailleBlock);
      scorePacman += 5000;
    }
    tableau_fruit[0]="x";
  }
  
  //
  // regarde si pacman recupere un powerUp
  if (parcourir_tableau(tableau_powerUp,pacman_x / tailleBlock,pacman_y / tailleBlock)) {
    replace(tableau_powerUp, pacman_x / tailleBlock, pacman_y / tailleBlock);
    powerUp = true;
    rouge.src = "assets/ghost_sprits/bad_ghost_b.png";
    bleu.src = "assets/ghost_sprits/bad_ghost_b.png";
    rose.src = "assets/ghost_sprits/bad_ghost_b.png";
    g_orange.src = "assets/ghost_sprits/bad_ghost_b.png";
  }

//
// dessine pacman avec son image

  context.drawImage(pacman,pacman_x, pacman_y, tailleBlock, tailleBlock);
  if (pacman_x < 0 && pacman_y == 12 * tailleBlock) {
    pacman_x = 25 * tailleBlock;
  }
  if (pacman_x > canvas.width && pacman_y == 12 * tailleBlock) {
    pacman_x = -1 * tailleBlock;
  }
  

  //
  //
  //gestion du score pour qu'il soit toujours centré sur le canvas
  context.fillStyle = "white";
  context.font = "20px Arial";
  if (scorePacman == 0) {
    context.fillText("Score : " + scorePacman, 9.9 * tailleBlock, 24.8 * 25); // avec un score avec 0
  } else if (scorePacman >= 10 && scorePacman < 100) {
    context.fillText("Score : " + scorePacman, 9.65 * tailleBlock, 24.8 * 25); // avec un score entre 10 et 100
  } else if (scorePacman >= 100 && scorePacman < 1000) {
    context.fillText("Score : " + scorePacman, 9.25 * tailleBlock, 24.8 * 25); // avec un score entre 100 et 1000
  } else if (scorePacman >= 1000 && scorePacman < 10000) {
    context.fillText("Score : " + scorePacman, 9.0 * tailleBlock, 24.8 * 25); // avec un score entre 1000 et 10000
  } else if (scorePacman>=10000 && scorePacman <100000){
    context.fillText("Score : " + scorePacman, 8.75 * tailleBlock, 24.8 * 25); // avec un score entre 10000 et 100 000
  } else{
    context.font = "18px Arial";                                              // avec un score superieur a 100 000
    context.fillText("Score :" + scorePacman,10.2*tailleBlock,23.8*25);
  }

  win();
  takePowerUp();
}
  //
  // fonction qui regarde si la prochaine case sur lequel pacman avance est vide ou non
function freeCase(direction) {
  if (direction == "left") {
    var x = pacman_x / tailleBlock - 1;
    var y = pacman_y / tailleBlock;
    var x_rouge = rouge_x / tailleBlock - 1;
    var y_rouge = rouge_y / tailleBlock;
    var x_bleu = bleu_x / tailleBlock - 1;
    var y_bleu = bleu_y / tailleBlock;
    var x_rose = rose_x / tailleBlock - 1;
    var y_rose = rose_y / tailleBlock;
    var x_orange = orange_x / tailleBlock - 1;
    var y_orange = orange_y / tailleBlock;
    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      parcourir_tableau(tableau_barriere, x, y) ||
      (x == x_rouge && y == y_rouge) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  } else if (direction == "right") {
    var x = pacman_x / tailleBlock + 1;
    var y = pacman_y / tailleBlock;
    var x_rouge = rouge_x / tailleBlock + 1;
    var y_rouge = rouge_y / tailleBlock;
    var x_bleu = bleu_x / tailleBlock + 1;
    var y_bleu = bleu_y / tailleBlock;
    var x_rose = rose_x / tailleBlock + 1;
    var y_rose = rose_y / tailleBlock;
    var x_orange = orange_x / tailleBlock + 1;
    var y_orange = orange_y / tailleBlock;
    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      parcourir_tableau(tableau_barriere, x, y) ||
      (x == x_rouge && y == y_rouge) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  } else if (direction == "up") {
    var x = pacman_x / tailleBlock;
    var y = pacman_y / tailleBlock - 1;
    var x_rouge = rouge_x / tailleBlock;
    var y_rouge = rouge_y / tailleBlock - 1;
    var x_bleu = bleu_x / tailleBlock;
    var y_bleu = bleu_y / tailleBlock - 1;
    var x_rose = rose_x / tailleBlock;
    var y_rose = rose_y / tailleBlock - 1;
    var x_orange = orange_x / tailleBlock;
    var y_orange = orange_y / tailleBlock - 1;

    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      parcourir_tableau(tableau_barriere, x, y) ||
      (x == x_rouge && y == y_rouge) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  } else if (direction == "down") {
    var x = pacman_x / tailleBlock;
    var y = pacman_y / tailleBlock + 1;
    var x_rouge = rouge_x / tailleBlock;
    var y_rouge = rouge_y / tailleBlock + 1;
    var x_bleu = bleu_x / tailleBlock;
    var y_bleu = bleu_y / tailleBlock + 1;
    var x_rose = rose_x / tailleBlock;
    var y_rose = rose_y / tailleBlock + 1;
    var x_orange = orange_x / tailleBlock;
    var y_orange = orange_y / tailleBlock + 1;
    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      parcourir_tableau(tableau_barriere, x, y) ||
      (x == x_rouge && y == y_rouge) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  }
  return true;
}

function freeCaseRouge(direction) {
  if (direction == "left") {
    var x = rouge_x / tailleBlock - 1;
    var y = rouge_y / tailleBlock;
    var x_bleu = bleu_x / tailleBlock - 1;
    var y_bleu = bleu_y / tailleBlock;
    var x_rose = rose_x / tailleBlock - 1;
    var y_rose = rose_y / tailleBlock;
    var x_orange = orange_x / tailleBlock - 1;
    var y_orange = orange_y / tailleBlock;
    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  } else if (direction == "right") {
    var x = rouge_x / tailleBlock + 1;
    var y = rouge_y / tailleBlock;
    var x_bleu = bleu_x / tailleBlock + 1;
    var y_bleu = bleu_y / tailleBlock;
    var x_rose = rose_x / tailleBlock + 1;
    var y_rose = rose_y / tailleBlock;
    var x_orange = orange_x / tailleBlock + 1;
    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  } else if (direction == "up") {
    var x = rouge_x / tailleBlock;
    var y = rouge_y / tailleBlock - 1;
    var x_bleu = bleu_x / tailleBlock;
    var y_bleu = bleu_y / tailleBlock - 1;
    var x_rose = rose_x / tailleBlock;
    var y_rose = rose_y / tailleBlock - 1;
    var x_orange = orange_x / tailleBlock;
    var y_orange = orange_y / tailleBlock - 1;
    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  } else if (direction == "down") {
    var x = rouge_x / tailleBlock;
    var y = rouge_y / tailleBlock + 1;
    var x_bleu = bleu_x / tailleBlock;
    var y_bleu = bleu_y / tailleBlock + 1;
    var x_rose = rose_x / tailleBlock;
    var y_rose = rose_y / tailleBlock + 1;
    var x_orange = orange_x / tailleBlock;
    var y_orange = orange_y / tailleBlock + 1;
    if (
      parcourir_tableau(bordure, x, y) ||
      parcourir_tableau(bordure_interne, x, y) ||
      (x == x_bleu && y == y_bleu) ||
      (x == x_rose && y == y_rose) ||
      (x == x_orange && y == y_orange)
    ) {
      return false;
    }
  }
  return true;
}

//
//fonction qui gere les mouvements avec les flèches directionnelles et "zqsd"
function mouvement(e) {
  if (e.code == "ArrowLeft" || e.code == "KeyA") {
    // si la touche gauche/Q est appuyée
    firstmove = true;
    pacman.src = "assets/pacman_sprits/pacman_o_l.png";
    vitesse_X = -1;
    vitesse_Y = 0;
  } else if (e.code == "ArrowUp" || e.code == "KeyW") {
    // si la touche haut/Z est appuyée
    firstmove = true;
    pacman.src = "assets/pacman_sprits/pacman_o_u.png";
    vitesse_Y = -1;
    vitesse_X = 0;
  } else if (e.code == "ArrowRight" || e.code == "KeyD") {
    // si la touche droite/D est appuyée
    firstmove = true;
    pacman.src = "assets/pacman_sprits/pacman_o_r.png";
    vitesse_X = 1;
    vitesse_Y = 0;
  } else if (e.code == "ArrowDown" || e.code == "KeyS") {
    // si la touche bas/S est appuyée
    firstmove = true;
    pacman.src = "assets/pacman_sprits/pacman_o_d.png";
    vitesse_Y = 1;
    vitesse_X = 0;
  }
}

//
//fonction qui parcours un tableau et renvoie true si il trouve la valeur chercher
function parcourir_tableau(tableau, x, y) {
  for (let i = 0; i < tableau.length; i++) {
    if (tableau[i][0] == x && tableau[i][1] == y) {
      return true;
    }
  }
}

//
//fonction qui parcours le tableau des fruits et renvoie true si il trouve la valeur chercher
function parcourir_tableau_fruit(tableau, x, y) {
  for (let i = 0; i < tableau.length; i++) {
    if (tableau[i][1] == x && tableau[i][2] == y) {
      return true;
    }
  }
}

//
//fonction qui renvoie l'index ou se trouve un valeur dans un tableau
function parcourir_tableau_index(tableau, x, y) {
  for (let i = 0; i < tableau.length; i++) {
    if (tableau[i][0] == x && tableau[i][1] == y) {
      return i;
    }
  }
}

//
//fonction qui remplace un element d'un tableau par "" a un index donné
function replace(tableau, x, y) {
  tableau[parcourir_tableau_index(tableau, x, y)] = "";
}

//
//fonction qui calcule le score selon si on prend un powerUp ou une nourriture simple
function score(tableau1, tableau2) {
  for (let i = 0; i < tableau1.length; i++) {
    if (tableau1[i] == "") {
      tableau1[i] = "x";
      scorePacman += 10;
    }
  }
  for (let i = 0; i < tableau2.length; i++) {
    if (tableau2[i] == "") {
      tableau2[i] = "x";
      scorePacman += 50;
    }
  }
  return scorePacman;
}
//
//fonction qui fait apparaitre un fruit aléatoire avec un timer qui ne gène pas le joueur
function spawnFruit() {
  if(tableau_fruit[0] == "x"){
    if (timerFruit == 0) {
      timerFruit = 20000;
      var random = Math.floor(Math.random() * 1000) + 1;
      if (random >= 1 && random <= 997) {
        var randomFruit = Math.floor(Math.random() * 4) + 1;
        if (randomFruit == 1) {
          fruit = "pomme";
          tableau_fruit = [[1,12,13]]
        } else if (randomFruit == 2) {
          fruit = "fraise";
          tableau_fruit = [[2,12,13]]
        } else if (randomFruit == 3) {
          fruit = "orange";
          tableau_fruit = [[3,12,13]]
        } else if (randomFruit == 4) {
          fruit = "cerise";
          tableau_fruit = [[4,12,13]]
        }
      } else if (random == 998) {
        fruit = "vaisseau";
        tableau_fruit = [[5,12,13]]
      } else if (random == 999) {
        fruit = "cloche";
        tableau_fruit = [[6,12,13]]
      } else {
        fruit = "clefs";
        tableau_fruit = [[7,12,13]]
      }
    } else {
      timerFruit -= 1;
    }
  }
}

function compter(){
  caseParcouru++;
  caseParcouru = caseParcouru % 3;
  if(vitesse_X == -1){
    if (caseParcouru == 0) {
      pacman.src = "assets/pacman_sprits/pacman_o_l.png";
    } else if (caseParcouru == 1) {
      pacman.src = "assets/pacman_sprits/pacman_mo_l.png";
    } else if (caseParcouru == 2) {
      pacman.src = "assets/pacman_sprits/pacman_f.png";
    }
  }
  else if (vitesse_X == 1) {
    if (caseParcouru == 0) {
      pacman.src = "assets/pacman_sprits/pacman_o_r.png";
    } else if (caseParcouru == 1) {
      pacman.src = "assets/pacman_sprits/pacman_mo_r.png";
    } else if (caseParcouru == 2) {
      pacman.src = "assets/pacman_sprits/pacman_f.png";
    }
  }
  else if (vitesse_Y == -1) {
    if (caseParcouru == 0) {
      pacman.src = "assets/pacman_sprits/pacman_o_u.png";
    } else if (caseParcouru == 1) {
      pacman.src = "assets/pacman_sprits/pacman_mo_u.png";
    } else if (caseParcouru == 2) {
      pacman.src = "assets/pacman_sprits/pacman_f.png";
    }
  }
  else if (vitesse_Y == 1) {
    if (caseParcouru == 0) {
      pacman.src = "assets/pacman_sprits/pacman_o_d.png";
    } else if (caseParcouru == 1) {
      pacman.src = "assets/pacman_sprits/pacman_mo_d.png";
    } else if (caseParcouru == 2) {
      pacman.src = "assets/pacman_sprits/pacman_f.png";
    }
  }
}

function pacmanTouchGhost(){
  if ((pacman_x/tailleBlock == rouge_x/tailleBlock && pacman_y/tailleBlock == rouge_y/tailleBlock)
  || (pacman_x/tailleBlock == bleu_x/tailleBlock && pacman_y/tailleBlock == bleu_y/tailleBlock)
  || (pacman_x/tailleBlock == rose_x/tailleBlock && pacman_y/tailleBlock == rose_y/tailleBlock)
  || (pacman_x/tailleBlock == orange_x/tailleBlock && pacman_y/tailleBlock == orange_y/tailleBlock)) {
    if (powerUp == false) {
      viePacman -= 1;
      if (viePacman == 2) {
        alert("You lost a life, you have " + viePacman + " lives left");
      }
      else if (viePacman == 1) {
        alert("You lost a life, you have " + viePacman + " life left");
      }
      //replace tout le monde a leur position de départ
      pacman_x = 12 * tailleBlock;
      pacman_y = 13 * tailleBlock;

      rouge_x = 11 * tailleBlock;
      rouge_y = 9 * tailleBlock;

      bleu_x = 10 * tailleBlock;
      bleu_y = 9 * tailleBlock;

      rose_x = 13 * tailleBlock;
      rose_y = 9 * tailleBlock;

      orange_x = 14 * tailleBlock;
      orange_y = 9 * tailleBlock;

      fantomeMaisonBleu = true;
      fantomeMaisonRouge = true;
      fantomeMaisonRose = true;
      fantomeMaisonOrange = true;
      timerRouge = 10;
      timerBleu = 20;
      timerRose = 40;
      timerOrange = 55;
      vitesse_X = 0;
      vitesse_Y = 0;
      firstmove = false;
      rouge.src = "assets/ghost_sprits/red_r.png";
      bleu.src = "assets/ghost_sprits/blue_r.png";
      rose.src = "assets/ghost_sprits/pink_l.png";
      g_orange.src = "assets/ghost_sprits/orange_l.png";
      powerUp = false;
      if (viePacman == 0) {
        end_game();
      }
    } 
    else{
      context.fillStyle = "white";
      context.font = "20px Arial";
      context.fillText("+200", rouge_x, (rouge_y/tailleBlock +1 ) * tailleBlock);
      scorePacman += 200;
      rouge_x = 11 * tailleBlock;
      rouge_y = 9 * tailleBlock;
      fantomeMaisonRouge = true;
      timerRouge = 3;
    }  
  }
}

// fonction qui fait sortir les fantomes de leur maison
function trouverDeplacementFantome(){
  if (fantomeMaisonRouge == true){
    chaseTargetRouge = [12,7];
    chase(chaseTargetRouge);
    if(rouge_x/tailleBlock == 12 && rouge_y/tailleBlock == 7){
      fantomeMaisonRouge = false;
    }
  }
  else if (fantomeMaisonRouge == false){
    if(pacman_x/tailleBlock == 5 && pacman_y/tailleBlock ==12 && rouge_x/tailleBlock == 19 && rouge_y/tailleBlock == 12){
      chaseTargetRouge = [18,11];
      chase(chaseTargetRouge,"rouge");
    }
    else if ((rouge_x/tailleBlock == 12 && rouge_y/tailleBlock == 7) && pacman_x/tailleBlock <=12){
      chase([7,7]);
    }
    else if ((rouge_x/tailleBlock == 7 && rouge_y/tailleBlock == 7) && pacman_x/tailleBlock <=12){
      chase([7,9]);
    }
    else if ((rouge_x/tailleBlock == 7 && rouge_y/tailleBlock == 9) && pacman_x/tailleBlock <=12){
      chase([8,9]);
    }
    else if ((rouge_x/tailleBlock == 8 && rouge_y/tailleBlock == 9) && pacman_x/tailleBlock <=12){
      chase([8,11]);
    }

    else if ((rouge_x/tailleBlock == 12 && rouge_y/tailleBlock == 7) && pacman_x/tailleBlock >12){
      chase([17,7]);
    }
    else if ((rouge_x/tailleBlock == 17 && rouge_y/tailleBlock == 7) && pacman_x/tailleBlock >12){
      chase([17,9]);
    }
    else if ((rouge_x/tailleBlock == 17 && rouge_y/tailleBlock == 9) && pacman_x/tailleBlock >12){
      chase([16,9]);
    }
    else if ((rouge_x/tailleBlock == 16 && rouge_y/tailleBlock == 9) && pacman_x/tailleBlock >12){
      chase([16,11]);
    }
    else{
      chaseTargetRouge = [pacman_x/tailleBlock,pacman_y/tailleBlock];
      chase(chaseTargetRouge,"rouge");
    }
  }
}


function chase(chaseTarget,fantome_couleur){
  if(fantome_couleur = "rouge"){
    if (chaseTarget[0] < rouge_x/tailleBlock && freeCaseRouge("left")) {
      rouge.src = "assets/ghost_sprits/red_l.png";
      rouge_vitesse_X = -1;
      rouge_vitesse_Y = 0;
    } else if (chaseTarget[0] > rouge_x/tailleBlock && freeCaseRouge("right")) {
      rouge.src = "assets/ghost_sprits/red_r.png";
      rouge_vitesse_X = 1;
      rouge_vitesse_Y = 0;
    } else if (chaseTarget[1] < rouge_y/tailleBlock && freeCaseRouge("up")) {
      rouge.src = "assets/ghost_sprits/red_u.png";
      rouge_vitesse_X = 0;
      rouge_vitesse_Y = -1;
    } else if (chaseTarget[1] > rouge_y/tailleBlock &&freeCaseRouge("down")) {
      rouge.src = "assets/ghost_sprits/red_d.png";
      rouge_vitesse_X = 0;
      rouge_vitesse_Y = 1;
    }
  }
}

function deplacementPacman(){
  if (freeCase("left") && vitesse_X == -1) {
    caseParcouru = caseParcouru % 3;
    pacman_x += vitesse_X * tailleBlock;
    pacman_y += vitesse_Y * tailleBlock;
  } else if (freeCase("right") && vitesse_X == 1) {
    pacman_x += vitesse_X * tailleBlock;
    pacman_y += vitesse_Y * tailleBlock;
  } else if (freeCase("up") && vitesse_Y == -1) {
    pacman_x += vitesse_X * tailleBlock;
    pacman_y += vitesse_Y * tailleBlock;
  } else if (freeCase("down") && vitesse_Y == 1) {
    pacman_x += vitesse_X * tailleBlock;
    pacman_y += vitesse_Y * tailleBlock;
  }
}

function deplacementFantome(){
  if (timerRouge > 0){
    fantomeMaisonRouge = true;
    timerRouge--;
  }
  /* si le timer est a 0, le fantome bleu se deplace */
  if (timerRouge == 0){
    if(freeCaseRouge("left") && rouge_vitesse_X == -1){
      rouge_x += rouge_vitesse_X * tailleBlock;
      rouge_y += rouge_vitesse_Y * tailleBlock;
    }else if(freeCaseRouge("right") && rouge_vitesse_X == 1){
      rouge_x += rouge_vitesse_X * tailleBlock;
      rouge_y += rouge_vitesse_Y * tailleBlock;
    }else if(freeCaseRouge("up") && rouge_vitesse_Y == -1){
      rouge_x += rouge_vitesse_X * tailleBlock;
      rouge_y += rouge_vitesse_Y * tailleBlock;
    }else if(freeCaseRouge("down") && rouge_vitesse_Y == 1){
      rouge_x += rouge_vitesse_X * tailleBlock;
      rouge_y += rouge_vitesse_Y * tailleBlock;
    }
  }
}

function win(){
  for (i=0;i<tableau_nourriture.length;i++){
    if (tableau_nourriture[i] == "x"){
      tableau_nourriture[i] = "xx"
      compteur++;
    }
  }
  if (compteur == tableau_nourriture.length){
    restart()
  }
}

function restart(){
   pacman_x = 12 * tailleBlock;
   pacman_y = 13 * tailleBlock;
   vitesse_X = 0;
   vitesse_Y = 0;
   firstmove = false;
   viePacman +=1;
   powerUp = false;
   timerPowerUp = 20000;
//r pour les fantomes globaux
   fantomeMaisonRouge = true;
   fantomeMaisonRose = true;
   fantomeMaisonBleu = true;
   fantomeMaisonOrange = true;
   rougeMaudit = true;
   bleuMaudit = true;
   orangeMaudit = true;
   roseMaudit = true;


  // var pour le fantome rouge
  rouge.src = "assets/ghost_sprits/red_r.png";
  rouge_x = 11 * tailleBlock;
  rouge_y = 9* tailleBlock;
  rouge_vitesse_X = 0;
  rouge_vitesse_Y = 0;
  timerRouge = 10;

  //var pour le fantome bleu
  bleu.src = "assets/ghost_sprits/blue_r.png";
  bleu_x = 10 * tailleBlock;
  bleu_y = 9 * tailleBlock;
  bleu_vitesse_X = 0;
  bleu_vitesse_Y = 0;
  timerBleu = 

  rose.src = "assets/ghost_sprits/pink_l.png";
  rose_x = 13 * tailleBlock;
  rose_y = 9 * tailleBlock;
  rose_vitesse_X = 0;
  rose_vitesse_Y = 0;
  timerRose = 

  g_orange.src = "assets/ghost_sprits/orange_l.png";
  orange_x = 14 * tailleBlock;
  orange_y = 9 * tailleBlock;
  orange_vitesse_X = 0;
  orange_vitesse_Y = 0;
  timerOrange = 
  compteur =0;
  timerFruit = 20000;

  tableau_nourriture = [[1, 1],[1, 2],[1, 3],[1, 4],[1, 5],[1, 6],[1, 7],[1, 8],[1, 9],[1, 15],[1, 16],[1, 17],[1, 18],[1, 19],[1, 20]
                        ,[1, 21],[1, 22],[1, 23],[2, 1],[2, 6],[2, 9],[2, 15],[2, 19],[2, 23],[3, 1],[3, 9],[3, 10],[3, 14],[3, 15],[3, 19]
                        ,[3, 23],[4, 1],[4, 6],[4, 15],[4, 19],[4, 23],[5, 1],[5, 2],[5, 3],[5, 4],[5, 5],[5, 6],[5, 7],[5, 8],[5, 9],[5, 10]
                        ,[5, 14],[5, 15],[5, 16],[5, 17],[5, 18],[5, 19],[5, 20],[5, 22],[5, 23],[6, 1],[6, 5],[6, 18],[6, 17],[6, 20],[6, 22]
                        ,[7, 1],[7, 5],[7, 7],[7, 8],[7, 9],[7, 17],[7, 20],[7, 19],[7, 22],[8, 1],[8, 5],[8, 7],[8, 9],[8, 10],[8, 14],[8, 15]
                        ,[8, 16],[8, 17],[8, 19],[8, 21],[8, 22],[9, 1],[9, 2],[9, 3],[9, 4],[9, 5],[9, 7],[9, 19],[9, 21],[10, 1],[10, 5],[10, 7]
                        ,[10, 9],[10, 14],[10, 15],[10, 16],[10, 17],[10, 18],[10, 19],[10, 21],[11, 1],[11, 2],[11, 3],[11, 4],[11, 5],[11, 7]
                        ,[11, 20],[11, 21],[12, 4],[12, 7],[12, 14],[12, 15],[12, 16],[12, 17],[12, 18],[12, 19],[12, 20],[13, 1],[13, 2],[13, 3]
                        ,[13, 4],[13, 5],[13, 7],[13, 20],[13, 21],[14, 1],[14, 5],[14, 7],[14, 9],[14, 14],[14, 15],[14, 16],[14, 17],[14, 18]
                        ,[14, 19],[14, 21],[15, 1],[15, 2],[15, 3],[15, 4],[15, 5],[15, 7],[15, 19],[15, 21],[16, 1],[16, 5],[16, 7],[16, 9]
                        ,[16, 10],[16, 14],[16, 15],[16, 16],[16, 17],[16, 19],[16, 21],[16, 22],[17, 1],[17, 5],[17, 7],[17, 8],[17, 9],[17, 17]
                        ,[17, 20],[17, 19],[17, 22],[18, 1],[18, 5],[18, 17],[18, 18],[18, 20],[18, 22],[19, 1],[19, 2],[19, 3],[19, 4],[19, 5]
                        ,[19, 6],[19, 7],[19, 8],[19, 9],[19, 10],[19, 14],[19, 15],[19, 16],[19, 17],[19, 18],[19, 19],[19, 20],[19, 22],[20, 1]
                        ,[20, 6],[20, 15],[20, 19],[19, 23],[20, 23],[21, 1],[21, 9],[21, 10],[21, 14],[21, 15],[21, 19],[21, 23],[22, 1],[22, 6]
                        ,[22, 9],[22, 15],[22, 19],[22, 23],[23, 1],[23, 2],[23, 3],[23, 4],[23, 5],[23, 6],[23, 7],[23, 8],[23, 9],[23, 15],[23, 16]
                        ,[23, 17],[23, 18],[23, 19],[23, 20],[23, 21],[23, 22],[23, 23],[3, 11],[3, 12],[3, 13],[4, 12],[5, 11],[5, 12],[5, 13],[6, 11]
                        ,[6, 13],[7, 11],[7, 13],[8, 11],[8, 13],[9, 11],[9, 13],[10, 11],[10, 13],[11, 11],[11, 13],[12, 11],[13, 11],[13, 13],[14, 11]
                        ,[14, 13],[15, 11],[15, 13],[16, 11],[16, 13],[17, 11],[17, 13],[18, 11],[18, 13],[19, 11],[19, 12],[19, 13],[20, 12],[21, 12]
                        ,[21, 11],[21, 13]];

    tableau_powerUp = [[3, 6],[21, 6],[19, 21],[5, 21]];
}
function takePowerUp(){
  if (powerUp){
    rouge.src = "assets/ghost_sprits/bad_ghost_b.png";
    console.log(timerPowerUp);
    timerPowerUp--;
    if (timerPowerUp == 0){
      timerPowerUp = 35;
      powerUp = false;
      bleu.src="assets/ghost_sprits/blue_r.png";
      rose.src="assets/ghost_sprits/pink_l.png";
      g_orange.src="assets/ghost_sprits/orange_l.png";
    }
  }
}
function set_highscore(){
  score1 = localStorage.getItem("score");
  score2 = localStorage.getItem("score2");
  score3 = localStorage.getItem("score3");
  score4 = localStorage.getItem("score4");
  score5 = localStorage.getItem("score5");

  name1 = localStorage.getItem("name");
  name2 = localStorage.getItem("name2");
  name3 = localStorage.getItem("name3");
  name4 = localStorage.getItem("name4");
  name5 = localStorage.getItem("name5");

  console.log(score1, score2, score3, score4, score5);
  console.log(scorePacman);
  console.log("");
  console.log(scorePacman > score1);
  console.log(scorePacman > score2);
  console.log(scorePacman > score3);
  console.log(scorePacman > score4);
  console.log(scorePacman > score5);

  if (scorePacman > score1){
    nomJoueur = prompt("You beat the first highscore! What's your name?");
    localStorage.setItem("score",scorePacman);
    localStorage.setItem("name",nomJoueur);
    localStorage.setItem("score2",score1);
    localStorage.setItem("name2",name1);
    localStorage.setItem("score3",score2);
    localStorage.setItem("name3",name2);
    localStorage.setItem("score4",score3);
    localStorage.setItem("name4",name3);
    localStorage.setItem("score5",score4);
    localStorage.setItem("name5",name4);
    score1= scorePacman;
    name1 = nomJoueur;
  }
  else if (scorePacman > score2 && scorePacman < score1){
    nomJoueur = prompt("You beat the second highscore! You are close to be the first! What's your name?");

    localStorage.setItem("score2",scorePacman);
    localStorage.setItem("name2",nomJoueur);
    localStorage.setItem("score3",score2);
    localStorage.setItem("name3",name2);
    localStorage.setItem("score4",score3);
    localStorage.setItem("name4",name3);
    localStorage.setItem("score5",score4);
    localStorage.setItem("name5",name4);
    score2= scorePacman;
    name2 = nomJoueur;
  }
  else if (scorePacman > score3 && scorePacman < score2 && scorePacman < score1){
    nomJoueur = prompt("You beat the third highscore. You are on the podium, congratulations! What's your name");

    localStorage.setItem("score3",scorePacman);
    localStorage.setItem("name3",nomJoueur);
    localStorage.setItem("score4",score3);
    localStorage.setItem("name4",name3);
    localStorage.setItem("score5",score4);
    localStorage.setItem("name5",name4);
    score3= scorePacman;
    name3 = nomJoueur;
  }
  else if (scorePacman > score4 && scorePacman < score3 && scorePacman < score2 && scorePacman < score1){
    nomJoueur = prompt("You beat the fourth highscore. You can be better than that, I saw the way you played! What's your name?");

    localStorage.setItem("score4",scorePacman);
    localStorage.setItem("name4",nomJoueur);
    localStorage.setItem("score5",score4);
    localStorage.setItem("name5",name4);
    score4= scorePacman;
    name4 = nomJoueur;
  }
  else if (scorePacman > score5 && scorePacman < score4 && scorePacman < score3 && scorePacman < score2 && scorePacman < score1){
    nomJoueur = prompt("You beat the fifth highscore. You are the best, I'm sure of it! What's your name?");

    localStorage.setItem("score5",scorePacman);
    localStorage.setItem("name5",nomJoueur);
    score5= scorePacman;
    name5 = nomJoueur;
  }

  console.log("apres les if");
  console.log(score1, score2, score3, score4, score5);
  console.log(name1, name2, name3, name4, name5);
  console.log(scorePacman);
  console.log("");
  console.log(scorePacman > score1);
  console.log(scorePacman > score2);
  console.log(scorePacman > score3);
  console.log(scorePacman > score4);
  console.log(scorePacman > score5);
}

function end_game(){
  alert("0 lifes left, you loose");

  pacman_x = 12 * tailleBlock;
   pacman_y = 13 * tailleBlock;
   vitesse_X = 0;
   vitesse_Y = 0;
   firstmove = false;
   viePacman +=1;
   powerUp = false;
   timerPowerUp = 20000;
//r pour les fantomes globaux
   fantomeMaisonRouge = true;
   fantomeMaisonRose = true;
   fantomeMaisonBleu = true;
   fantomeMaisonOrange = true;
   rougeMaudit = true;
   bleuMaudit = true;
   orangeMaudit = true;
   roseMaudit = true;


  // var pour le fantome rouge
  rouge.src = "assets/ghost_sprits/red_r.png";
  rouge_x = 11 * tailleBlock;
  rouge_y = 9* tailleBlock;
  rouge_vitesse_X = 0;
  rouge_vitesse_Y = 0;
  timerRouge = 10;

  //var pour le fantome bleu
  bleu.src = "assets/ghost_sprits/blue_r.png";
  bleu_x = 10 * tailleBlock;
  bleu_y = 9 * tailleBlock;
  bleu_vitesse_X = 0;
  bleu_vitesse_Y = 0;
  timerBleu = 

  rose.src = "assets/ghost_sprits/pink_l.png";
  rose_x = 13 * tailleBlock;
  rose_y = 9 * tailleBlock;
  rose_vitesse_X = 0;
  rose_vitesse_Y = 0;
  timerRose = 

  g_orange.src = "assets/ghost_sprits/orange_l.png";
  orange_x = 14 * tailleBlock;
  orange_y = 9 * tailleBlock;
  orange_vitesse_X = 0;
  orange_vitesse_Y = 0;
  timerOrange = 
  compteur =0;
  timerFruit = 20000;

  tableau_nourriture = [[1, 1],[1, 2],[1, 3],[1, 4],[1, 5],[1, 6],[1, 7],[1, 8],[1, 9],[1, 15],[1, 16],[1, 17],[1, 18],[1, 19],[1, 20]
                        ,[1, 21],[1, 22],[1, 23],[2, 1],[2, 6],[2, 9],[2, 15],[2, 19],[2, 23],[3, 1],[3, 9],[3, 10],[3, 14],[3, 15],[3, 19]
                        ,[3, 23],[4, 1],[4, 6],[4, 15],[4, 19],[4, 23],[5, 1],[5, 2],[5, 3],[5, 4],[5, 5],[5, 6],[5, 7],[5, 8],[5, 9],[5, 10]
                        ,[5, 14],[5, 15],[5, 16],[5, 17],[5, 18],[5, 19],[5, 20],[5, 22],[5, 23],[6, 1],[6, 5],[6, 18],[6, 17],[6, 20],[6, 22]
                        ,[7, 1],[7, 5],[7, 7],[7, 8],[7, 9],[7, 17],[7, 20],[7, 19],[7, 22],[8, 1],[8, 5],[8, 7],[8, 9],[8, 10],[8, 14],[8, 15]
                        ,[8, 16],[8, 17],[8, 19],[8, 21],[8, 22],[9, 1],[9, 2],[9, 3],[9, 4],[9, 5],[9, 7],[9, 19],[9, 21],[10, 1],[10, 5],[10, 7]
                        ,[10, 9],[10, 14],[10, 15],[10, 16],[10, 17],[10, 18],[10, 19],[10, 21],[11, 1],[11, 2],[11, 3],[11, 4],[11, 5],[11, 7]
                        ,[11, 20],[11, 21],[12, 4],[12, 7],[12, 14],[12, 15],[12, 16],[12, 17],[12, 18],[12, 19],[12, 20],[13, 1],[13, 2],[13, 3]
                        ,[13, 4],[13, 5],[13, 7],[13, 20],[13, 21],[14, 1],[14, 5],[14, 7],[14, 9],[14, 14],[14, 15],[14, 16],[14, 17],[14, 18]
                        ,[14, 19],[14, 21],[15, 1],[15, 2],[15, 3],[15, 4],[15, 5],[15, 7],[15, 19],[15, 21],[16, 1],[16, 5],[16, 7],[16, 9]
                        ,[16, 10],[16, 14],[16, 15],[16, 16],[16, 17],[16, 19],[16, 21],[16, 22],[17, 1],[17, 5],[17, 7],[17, 8],[17, 9],[17, 17]
                        ,[17, 20],[17, 19],[17, 22],[18, 1],[18, 5],[18, 17],[18, 18],[18, 20],[18, 22],[19, 1],[19, 2],[19, 3],[19, 4],[19, 5]
                        ,[19, 6],[19, 7],[19, 8],[19, 9],[19, 10],[19, 14],[19, 15],[19, 16],[19, 17],[19, 18],[19, 19],[19, 20],[19, 22],[20, 1]
                        ,[20, 6],[20, 15],[20, 19],[19, 23],[20, 23],[21, 1],[21, 9],[21, 10],[21, 14],[21, 15],[21, 19],[21, 23],[22, 1],[22, 6]
                        ,[22, 9],[22, 15],[22, 19],[22, 23],[23, 1],[23, 2],[23, 3],[23, 4],[23, 5],[23, 6],[23, 7],[23, 8],[23, 9],[23, 15],[23, 16]
                        ,[23, 17],[23, 18],[23, 19],[23, 20],[23, 21],[23, 22],[23, 23],[3, 11],[3, 12],[3, 13],[4, 12],[5, 11],[5, 12],[5, 13],[6, 11]
                        ,[6, 13],[7, 11],[7, 13],[8, 11],[8, 13],[9, 11],[9, 13],[10, 11],[10, 13],[11, 11],[11, 13],[12, 11],[13, 11],[13, 13],[14, 11]
                        ,[14, 13],[15, 11],[15, 13],[16, 11],[16, 13],[17, 11],[17, 13],[18, 11],[18, 13],[19, 11],[19, 12],[19, 13],[20, 12],[21, 12]
                        ,[21, 11],[21, 13]];

    tableau_powerUp = [[3, 6],[21, 6],[19, 21],[5, 21]];

    set_highscore();
    scorePacman = 0;
    //met le canvas en display none
    document.getElementById("canvas").style.display = "none";
    document.getElementById("menu").style.display = "flex";
    //remove class zoom to volumeC and musiqueC
    document.getElementById("sonsC").classList.remove("zoom");
    document.getElementById("musiqueC").classList.remove("zoom");
    document.getElementById("logo").classList.remove("zoom");
    //recupere pacman et le met en display flex
    document.getElementById("pacC").style.width = "100%";
    document.getElementById("pacC").style.height = "100%";
    document.getElementById("pac").style.width = "10%";

    document.getElementById("jeu").style.display = "none";
    viePacman = 3;
}