let menuMusic = new Audio("assets/sounds/menuMusic.wav");
let jingleDebut = new Audio("assets/sounds/pacman_beginning.wav");
let waka = new Audio("assets/sounds/wakat.wav");
let click = new Audio("assets/sounds/click.wav");
let hover = new Audio("assets/sounds/hover.wav");

spritePac = document.getElementById("pac");
spriteRed = document.getElementById('red');
spriteBlue = document.getElementById('blue');
spritePink = document.getElementById('pink');
spriteOrange = document.getElementById('orange');

var page = "";

var framesRed = [
        'assets/ghost_sprits/row-1-column-1.png',
        "assets/ghost_sprits/row-1-column-2.png"
    ];
var framesBlue = [
        'assets/ghost_sprits/row-3-column-2.png',
        "assets/ghost_sprits/row-3-column-1.png"
    ];
var framesPink = [
        'assets/ghost_sprits/row-4-column-3.png',
        "assets/ghost_sprits/row-4-column-4.png"
    ];
var framesOrange = [
        'assets/ghost_sprits/row-3-column-7.png',
        "assets/ghost_sprits/row-3-column-8.png"
    ];
var framesPac = [
        'assets/pacman_sprits/pacman_f.png'
    ];
    

function hudAppa(){
    var elem = document.getElementById("borneC");
    elem.classList.add('borneAnim');
    
    setTimeout(function() {

        var elem = document.getElementById("borneC");
        elem.style.top = '-100vh';

        jingleDebut.volume = 1;
        jingleDebut.pause();
        jingleDebut.play();
        
        var elem = document.getElementById("hudC");
        elem.classList.add('coteAnim');

        setTimeout(function() {
            var elem = document.getElementById("hudC");
            elem.style.top = '0vh';

            var elem = document.getElementById("ecranC");
            elem.classList.add('ecranAnim');

            setTimeout(function() {
                var elem = document.getElementById("ecranC");
                elem.style.top = '0vh';

                var video = document.getElementById("transition");
                video.play();
                video.style.display = "flex";

                setTimeout(function() {
                    video.style.display = "none";
                    document.querySelector("body").style.backgroundColor = "#000000" ;
                    document.getElementById("logoC").style.display = "flex";
                    document.getElementById('logo').classList.add('zoom');
                    document.getElementById("menuImgC").style.display = "flex";
                    document.getElementById('menuImg').classList.add('zoomMenu');

                    setTimeout(function() {
                        document.getElementById('logo').style.width = '18%';
                        document.getElementById('menuImg').style.width = '50%';
                        var anim = document.getElementById('chargeAnimC');
                        var img = document.getElementById('chargeAnim');
                        anim.classList.add('charge');
                        setTimeout(function() {
                            document.getElementById("chargeAnimC").style.display = "flex";
                            pacmanAnim(img,anim);
                        },100);
                    },616);

                },2000);

            },1000);

        },1000);
        
        }, 700);
    }

function pacmanAnim(img,anim){
    waka.volume = 1;
    waka.pause();
    waka.play();
    img.setAttribute('src', 'assets/pacman_sprits/pacman_mo_r.png');
        setTimeout(function() {
            img.setAttribute('src', 'assets/pacman_sprits/pacman_f.png');
            setTimeout(function() {
                img.setAttribute('src', 'assets/pacman_sprits/pacman_mo_r.png');
                setTimeout(function() {
                    img.setAttribute('src', 'assets/pacman_sprits/pacman_o_r.png');
                    setTimeout(function() {
                        img.setAttribute('src', 'assets/pacman_sprits/pacman_mo_r.png');
                        setTimeout(function() {
                            img.setAttribute('src', 'assets/pacman_sprits/pacman_f.png');
                            setTimeout(function() {
                                img.setAttribute('src', 'assets/pacman_sprits/pacman_mo_r.png');
                                setTimeout(function() {
                                    img.setAttribute('src', 'assets/pacman_sprits/pacman_o_r.png');
                                    setTimeout(function() {
                                        img.setAttribute('src', 'assets/pacman_sprits/pacman_mo_r.png');
                                        setTimeout(function() {
                                            img.setAttribute('src', 'assets/pacman_sprits/pacman_f.png');
                                            setTimeout(function() {
                                                img.setAttribute('src', 'assets/pacman_sprits/pacman_mo_r.png');
                                                setTimeout(function() {
                                                    img.setAttribute('src', 'assets/pacman_sprits/pacman_o_r.png');
                                                    setTimeout(function() {
                                                        img.setAttribute('src', 'assets/pacman_sprits/pacman_mo_r.png');
                                                        setTimeout(function() {
                                                            img.setAttribute('src', 'assets/pacman_sprits/pacman_f.png');
                                                            anim.style.right="0vh";

                                                                setTimeout(function() {
                                                                    document.getElementById("pacC").style.display = "flex";
                                                                    img.style.display = "none";
                                                                    fantomesAppa();
                                                                }, 500);

                                                            },100);
                                                        },100);
                                                    },100);
                                                },100);
                                            },100);
                                        },100);
                                    },100);
                                },100);
                            },100);
                        },100);
                    },100);
                },100);
            },100);

}

function fantomesAppa(){
    var redC = document.getElementById("redC");
    var pinkC = document.getElementById("pinkC");
    var blueC = document.getElementById("blueC");
    var orangeC = document.getElementById("orangeC");

    redC.style.display = "flex";
    pinkC.style.display = "flex";

    redC.classList.add('fan-gau');
    pinkC.classList.add('fan-gau');

    menuMusic.volume = 1;
    menuMusic.pause();
    menuMusic.play();
    menuMusic.loop = true;

    setTimeout(function() {

        redC.style.right="130vh";
        pinkC.style.right="130vh";

        setTimeout(function() {

            blueC.style.display = "flex";
            orangeC.style.display = "flex";

            blueC.classList.add('fan-dro');
            orangeC.classList.add('fan-dro');

            setTimeout(function() {

                blueC.style.left="130vh";
                orangeC.style.left="130vh";

                setTimeout(function() {
                    volumeAppa();
                },600);

            },290);

        }, 620);

    },270);
}

function volumeAppa(){
    var sonsC = document.getElementById("sonsC");
    sonsC.style.display = "flex";
    sonsC.classList.add('zoom');

    var musiqueC = document.getElementById("musiqueC");
    musiqueC.style.display = "flex";
    musiqueC.classList.add('zoom');

    document.getElementById('menuBImgC').style.display = "flex";
    document.getElementById('menuBImg').classList.add('zoomMenu');

    setTimeout(function() {
        document.getElementById("sons").style.width="49%";
        sonsC.style.bottom="87%";

        document.getElementById("musique").style.width="49%";
        musiqueC.style.bottom="87%";

        document.getElementById('menuBImg').style.width = '50%';
    },610);
}

function changeVol() {
    if(document.getElementById("sons").getAttribute("src") == "assets/clickable/volume.png"){

        click.muted=true;
        hover.muted=true;
        document.getElementById("sons").src = "assets/clickable/pas_volume.png";

    }else{

        click.muted=false;
        hover.muted=false;
        document.getElementById("sons").src = "assets/clickable/volume.png";
    }
}

function changeMus(){
    if(document.getElementById("musique").getAttribute("src") == "assets/clickable/musique.png"){

        menuMusic.pause();
        document.getElementById("musique").src = "assets/clickable/pas_musique.png";

    }else{

        menuMusic.play();
        document.getElementById("musique").src = "assets/clickable/musique.png";
    }
}

function back(){
    click.volume = 1;
    click.pause();
    click.play();

    if(page == "pink"){
        page = "";
        intervalPink = setInterval(changePinkFrames, 200);
        document.getElementById("arrowC").style.display = "none";
        document.getElementById("pinkC").style.pointerEvents = "auto";
        setTimeout(function(){
            document.getElementById("rulesC").classList.add("rulesDown");
            document.getElementById("rulesC").classList.remove("rulesUp");
            setTimeout(function(){
                document.getElementById("rulesC").style.top = "100vh";
            },1490);
        }, 400);
    }
    else if(page == "red"){
        page = "";
        intervalRed = setInterval(changeRedFrames, 200);
        document.getElementById("arrowC").style.display = "none";
        document.getElementById("redC").style.pointerEvents = "auto";
        setTimeout(function(){
            document.getElementById("creditsC").classList.add("creditsOut");
            document.getElementById("creditsC").classList.remove("credits");
            setTimeout(function(){
                document.getElementById("creditsC").style.left = "100%";
            },1490);
        }, 400);
    }
    else if(page == "orange"){
        page = "";
        intervalOrange = setInterval(changeOrangeFrames, 200);
        document.getElementById("arrowC").style.display = "none";
        document.getElementById("orangeC").style.pointerEvents = "auto";
        setTimeout(function(){
            document.getElementById("scoresC").classList.add("scoresOut");
            document.getElementById("scoresC").classList.remove("scores");
            document.getElementById("j1C").style.display="none";
            document.getElementById("j2C").style.display="none";
            document.getElementById("j3C").style.display="none";
            document.getElementById("j4C").style.display="none";
            document.getElementById("j5C").style.display="none";
            setTimeout(function(){
                document.getElementById("scoresC").style.bottom = "100%";
            },1490);
        }, 400);
    }
}

 function changeRedFrames(){
    spriteRed.src = framesRed.reverse()[0];
 }

function changeBlueFrames(){
    spriteBlue.src = framesBlue.reverse()[0];
}

function changePinkFrames(){
    spritePink.src = framesPink.reverse()[0];
}

function changeOrangeFrames(){
    spriteOrange.src = framesOrange.reverse()[0];
}

function changePacFrames(){
    spritePac.src = framesPac.reverse()[0];
}

intervalRed = setInterval(changeRedFrames, 200);
intervalBlue = setInterval(changeBlueFrames, 200);
intervalPink = setInterval(changePinkFrames, 200);
intervalOrange = setInterval(changeOrangeFrames, 200);
intervalPac = setInterval(changePacFrames, 200);


//rouge
spriteRed.addEventListener('mouseenter', (event) => {
    spriteRed.style.cursor = "pointer"
    framesRed = [
        'assets/ghost_sprits/row-5-column-3.png',
        "assets/ghost_sprits/row-5-column-6.png"

    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/MenuOBas.png");
    document.getElementById("titre").innerHTML = "CREDITS";
    document.getElementById("titre").style.color="#fa0000";
    hover.pause();
    hover.play();
});
spriteRed.addEventListener('mouseleave', (event) => {
    spriteRed.style.cursor = "default"
    framesRed = [
        'assets/ghost_sprits/row-1-column-1.png',
        "assets/ghost_sprits/row-1-column-2.png"
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/Menu.png");
    document.getElementById("titre").innerHTML = "";
});
spriteRed.addEventListener('click', (event) => {
    page = "red";
    click.volume = 1;
    click.pause();
    click.play();

    spriteRed.setAttribute("src", "assets/ghost_sprits/pop.png");
    clearInterval(intervalRed);
    document.getElementById("redC").style.pointerEvents = "none";

    setTimeout(function(){
        spriteRed.setAttribute("src", "");
        document.getElementById("creditsC").classList.add("credits");
        document.getElementById("creditsC").classList.remove("creditsOut");

        setTimeout(function(){
            document.getElementById("creditsC").style.left = "26%";
            setTimeout(function(){
                document.getElementById("arrowC").style.display = "flex";
            }
            , 200);
        },1490);
    }, 400);
});

//bleu
spriteBlue.addEventListener('mouseenter', (event) => {
    spriteBlue.style.cursor = "pointer"
    framesBlue = [
        'assets/ghost_sprits/row-5-column-3.png',
        "assets/ghost_sprits/row-5-column-6.png"
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/MenuOBas.png");
    document.getElementById("titre").innerHTML = "EXIT";
    document.getElementById("titre").style.color="#00fafa";
    hover.pause();
    hover.play();
});
spriteBlue.addEventListener('mouseleave', (event) => {
    spriteBlue.style.cursor = "default"
    framesBlue = [
        'assets/ghost_sprits/row-3-column-2.png',
        "assets/ghost_sprits/row-3-column-1.png"
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/Menu.png");
    document.getElementById("titre").innerHTML = "";
});
spriteBlue.addEventListener('click', (event) => {
    click.volume = 1;
    click.pause();
    click.play();

    spriteBlue.setAttribute("src", "assets/ghost_sprits/pop.png");
    clearInterval(intervalBlue);
    document.getElementById("blueC").style.pointerEvents = "none";

    document.getElementById("exitC").classList.add("exit");

    setTimeout(function(){
        spriteBlue.setAttribute("src", "");
    }, 400);

    setTimeout(function(){
        document.getElementById("exitC").style.right = "25%";
        setTimeout(function(){
            document.location.reload();
        }
        , 1600);
    },1400);
});


//rose
spritePink.addEventListener('mouseenter', (event) => {
    page = "pink";
    spritePink.style.cursor = "pointer"
    framesPink = [
        'assets/ghost_sprits/row-5-column-3.png',
        "assets/ghost_sprits/row-5-column-6.png"
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/MenuOBas.png");
    document.getElementById("titre").innerHTML = "RULES";
    document.getElementById("titre").style.color="#fa8fe2";
    hover.volume = 0.5;
    hover.pause();
    hover.play();
}
);
spritePink.addEventListener('mouseleave', (event) => {
    spritePink.style.cursor = "default"
    framesPink = [
        'assets/ghost_sprits/row-4-column-3.png',
        "assets/ghost_sprits/row-4-column-4.png"
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/Menu.png");
    document.getElementById("titre").innerHTML = "";
}
);
spritePink.addEventListener('click', (event) => {
    click.volume = 1;
    click.pause();
    click.play();

    spritePink.setAttribute("src", "assets/ghost_sprits/pop.png");
    clearInterval(intervalPink);
    document.getElementById("pinkC").style.pointerEvents = "none";
    setTimeout(function(){
        spritePink.setAttribute("src", "");
        document.getElementById("rulesC").classList.add("rulesUp");
        document.getElementById("rulesC").classList.remove("rulesDown");

        setTimeout(function(){
            document.getElementById("rulesC").style.top = "3.5%";
            setTimeout(function(){
                document.getElementById("arrowC").style.display = "flex";
            }
            , 200);
        },1490);
    }, 400);
});


//orange
spriteOrange.addEventListener('mouseenter', (event) => {
    spriteOrange.style.cursor = "pointer"
    framesOrange = [
        'assets/ghost_sprits/row-5-column-3.png',
        "assets/ghost_sprits/row-5-column-6.png"
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/MenuOBas.png");
    document.getElementById("titre").innerHTML = "SCORES";
    document.getElementById("titre").style.color="#fa8c05";
    hover.pause();
    hover.play();
}
);
spriteOrange.addEventListener('mouseleave', (event) => {
    spriteOrange.style.cursor = "default"
    framesOrange = [
        'assets/ghost_sprits/row-3-column-7.png',
        "assets/ghost_sprits/row-3-column-8.png"
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/Menu.png");
    document.getElementById("titre").innerHTML = "";
});
spriteOrange.addEventListener('click', (event) => {
    page = "orange";
    click.volume = 1;
    click.pause();
    click.play();
    
    spriteOrange.setAttribute("src", "assets/ghost_sprits/pop.png");
    clearInterval(intervalOrange);
    document.getElementById("orangeC").style.pointerEvents = "none";
    setTimeout(function(){
        spriteOrange.setAttribute("src", "");
        document.getElementById("scoresC").classList.add("scores");
        document.getElementById("scoresC").classList.remove("scoresOut");
        setTimeout(function(){
            document.getElementById("scoresC").style.bottom = "3.5%";
            setTimeout(function(){
                document.getElementById("arrowC").style.display = "flex";
                setTimeout(function(){
                    if(name1== ""){
                        name1 = "Vide"
                    }
                    document.getElementById("j1").innerHTML = name1 + " : " + score1;
                    document.getElementById("j1C").style.display="flex";

                    setTimeout(function(){
                        if(name2== ""){
                            name2 = "Vide"
                        }
                        document.getElementById("j2").innerHTML = name2 + " : " + score2;
                        document.getElementById("j2C").style.display="flex";
                        
                        setTimeout(function(){
                            if(name3== ""){
                                name3 = "Vide"
                            }
                            document.getElementById("j3").innerHTML = name3 + " : " + score3;
                            document.getElementById("j3C").style.display="flex";

                            setTimeout(function(){
                                if(name4== ""){
                                    name4 = "Vide"
                                }
                                document.getElementById("j4").innerHTML = name4 + " : " + score4;
                                document.getElementById("j4C").style.display="flex";
                                
                                setTimeout(function(){
                                    if(name5== ""){
                                        name5 = "Vide"
                                    }
                                    document.getElementById("j5").innerHTML = name5 + " : " + score5;
                                    document.getElementById("j5C").style.display="flex";

                                },1000);

                            },1000);

                        },1000);

                    },1000);

                },500);
            }
            , 200);
        },1490);
    }, 400);
});


//pacman
spritePac.addEventListener('mouseenter', (event) => {
    spritePac.style.cursor = "pointer"
    framesPac = [
        'assets/pacman_sprits/pacman_mo_u.png',
        "assets/pacman_sprits/pacman_o_u.png",
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/MenuOBas.png");
    document.getElementById("titre").innerHTML = "PLAY !";
    document.getElementById("titre").style.color="#ffff00";
});
spritePac.addEventListener('mouseleave', (event) => {
    spritePac.style.cursor = "default"
    framesPac = [
        'assets/pacman_sprits/pacman_f.png'
    ];
    document.getElementById("menuImg").setAttribute("src", "assets/p_accueil/Menu.png");
    document.getElementById("titre").innerHTML = "";
});
spritePac.addEventListener('click', (event) => {
    clearInterval(intervalPac);
    spritePac.setAttribute("src", "assets/pacman_sprits/pacman_f.png");
    click.volume = 1;
    click.pause();
    click.play();

    document.getElementById("pacC").style.zIndex = "-966";
    spritePac.style.cursor = "default"
    document.getElementById("pacC").setAttribute("pointer-events", "none");
    spritePac.classList.add("zoomPac");
    setTimeout(function () {
        document.getElementById("pacC").style.height="100%";
        document.getElementById("pacC").style.width="100%";
        document.getElementById("jeu").style.display="flex";
        document.getElementById("canvas").style.display = "flex";
        document.getElementById("menu").style.display="none";
        spritePac.style.width = "1000%";

        setTimeout(function () {
            document.getElementById("pacC").style.top = "3%";
            spritePac.classList.remove("zoomPac");
            spritePac.classList.add("zoomPacOut");

            setTimeout(function () {
                document.getElementById("pacC").style.width="0%";
                document.getElementById("pacC").style.zIndex = "-999";
                spritePac.classList.remove("zoomPacOut");
                intervalPac = setInterval(changePacFrames, 200);
                document.getElementById("pacC").setAttribute("pointer-events", "none");
            }, 1400);
        }, 500);
    }, 1000);
});